Mateusz Tokarz i Gabriela Czarska "Pirki"
Zalozenia podstawowe:
-Strona startowa i funkcje odpalajace/konczace gre -Tokarz
-Ulepszenia designu strony startowej - Czarska
-Statek -Czarska
-Tablica rybek -Czarska
-Kolorowanie   -Czarska
-Funkcja odpowiadajaca za najlepszy wynik - Tokarz
-Przeliczanie danych -Tokarz
-Tester złapania ryby -Tokarz
-Symulacja ruchu wedki za pomoca strzalki (w celu testowania podstaw gry) -Tokarz (edit/wczesniej bylo morza)
-Symulacja sprawdzenia czy ryba jest pod statkiem za pomocą wciśnięcia klawisza -Tokarz
Zalozenia rozszerzone:
-Licznik punkt
-Ruch morze (edit/wczesniej bylo wedki) -Tokarz i Czarska
-Ruch rybek - Tokarz i Czarska
-Zamiana statkow przy osiagnieciu odpowiedniej ilosci punktow - Czarska
-Pirania = animimacje porazki (game over) -Czarska
-Dopracowanie i skoordynowanie animacji wedki z ruchem ryb - Tokarz