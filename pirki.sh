#/bin/bash
#Mateusz Tokarz i Gabriela Czarska "Pirki"
#Gra Pirki realizowana jako projekt na Srodowisko Programisty TCS
printf '\e[8;30;120t'                  #ustawia wielkosc termianala na 30x120
N=120;                                 #szerokosc terminala
M=30;                                  #wysokosc terminala
tput bold                              #pogrubia znaki
tput civis                             #sprawia ze nie wyswietla sie jakis syf
licznik=0;                             #trzyma ilosc rozegranych gier
declare -a matrix                      #tablica wynikow
wynmax=0;                              #najlepszy wynik
declare -a tabtest                     #tablica testowa
czyryb=0;                              #bool do funkcji sprawdzjacej czy zlowiono rybke
wymiar=20;                             #cos do testow
rybki=0;                               #zlicza ilosc rybek w aktualnej rozgrywce
maxrybki=0;                            #trzyma najlepszy wynik
licznik=0;                             #trzyma ilosc rozegranych gier
declare -a matrix                      #tablica wynikow
wynmax=0;                              #najlepszy wynik
declare -a tablica                     #statek
declare -a tabmorze                    #morze
declare -a nosyryb                     #chyba rysowanie rybki
declare -a nosypirani                  #noski piranek
declare -a tabkoniec                   #animacja gameover
ileryb=0
ilepirek=0;
znak="y";
trybwedki=0;
xwed=10;
ywed=71;
lll=0;
lpl=0;
function czyrybka()                    #funkcja zajmujaca sie tym czy zlowimy rybke
{
czyryb=0;
mini=10000;
licz=1;
for ((i=191;i<2300;i=$i+120)) do
    if [ $czyryb -eq 0 ];then
        if [ "${tabmorze[$i+$licz]}" != "~" ];then
            czyryb=1;
            mini=$i;
        fi
    fi
    licz=$[licz+1]
done
}
wypisz()                               #funkcja wypisujaca ekran poczatkowy
{
    clear
    echo -e "\033[48;5;153m \033[38;5;172m                                                                                                                   "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                           IIIIIIIIIII      IIIII   IIIIIIIIIII      IIIII     IIIII   IIIII                            "
    echo "                           IIIIIIIIIIII     IIIII   IIIIIIIIIIII     IIIII    IIIII    IIIII                            "
    echo "                           IIIII    IIII    IIIII   IIIII    IIII    IIIII   IIIII     IIIII                            "
    echo "                           IIIII     IIII   IIIII   IIIII     IIII   IIIII  IIIII      IIIII                            "
    echo "                           IIIII     IIII   IIIII   IIIII     IIII   IIIII IIIII       IIIII                            "
    echo "                           IIIII     IIII   IIIII   IIIII     IIII   IIIIIIIIII        IIIII                            "
    echo "                           IIIII    IIII    IIIII   IIIII    IIII    IIIIIIIII         IIIII                            "
    echo "                           IIIIIIIIIIII     IIIII   IIIIIIIIIIII     IIIIIIIIII        IIIII                            "
    echo "                           IIIIIIIIII       IIIII   IIIIIIIIII       IIIII IIIII       IIIII                            "
    echo "                           IIIIIII          IIIII   IIIIIIIIIII      IIIII  IIIII      IIIII                            "
    echo "                           IIIII            IIIII   IIIII   IIIII    IIIII   IIIII     IIIII                            "
    echo "                           IIIII            IIIII   IIIII    IIIII   IIIII    IIIII    IIIII                            "
    echo "                           IIIII            IIIII   IIIII     IIIII  IIIII     IIIII   IIIII                            "
    echo -e "\033[48;5;153m \033[38;5;24m                                                                                                                       "
    echo "                                   ___                Witaj rybaku!                 ___                                 "
    echo "                                |||  .|>                                          <|.  |||                              "
    echo "                                   ~~~                                              ~~~                                 " 
    echo "                                  MATI                                              GABA                                "
    echo "                                                                                                                        "
    echo "               ZAGRAJ   (kliknij 1)   Sprawdz swoj najlepszy wynik   (kliknij 2)   Wyjdz z gry   (kliknij 3)            "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
}
function napisgameover()
{
    clear
    echo -e "\033[48;5;153m \033[38;5;172m                                                                                                                   "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                        _______             ____            ____         _____    _______________                       "
    echo "                       /      /\           /    /\         /   /\       /    /|  /              /|                      "
    echo "                       IIIIIII  \          IIIII \         IIIII \      IIIII |  IIIIIIIIIIIIIII |                      "
    echo "                     IIIIIIIIIII \         IIIII  \        IIIIII \    IIIIII |  IIIIIIIIIIIIIII |                      "
    echo "                    IIIIIIIIIIIII |       IIIIIII \        IIIIIII \  IIIIIII |  IIIIIIIIIIIIIII/                       "
    echo "                   IIIII |  IIIII |       IIIIIII  \       IIIIIII  \ IIIIIII |  IIIII |                                "
    echo "                   IIIII |  IIIII/       IIII IIII \       IIIIIIII  IIIIIIII |  IIIII |_________                       "
    echo "                   IIIII | _______       IIII IIII  \      IIIIIIIIIIIIIIIIII |  IIIII/         /|                      "
    echo "                   IIIII |/      /|     IIII |_IIII \      IIIII IIIIII IIIII |  IIIIIIIIIIIIIII |                      "
    echo "                   IIIII |IIIIIII |     IIII/  IIII  \     IIIII | III  IIIII |  IIIIIIIIIIIIIII/                       "
    echo "                   IIIII |IIIIIII |    IIIIIIIIIIIII \     IIIII |      IIIII |  IIIII |                                "
    echo "                   IIIII |__IIIII |    IIIIIIIIIIIII  \    IIIII |      IIIII |  IIIII |_________                       "
    echo "                   IIIII/   IIIII |   IIII /     IIII \    IIIII |      IIIII |  IIIII/         /|                      "
    echo "                    IIIIIIIIIIII /    IIII/      IIII  \   IIIII |      IIIII |  IIIIIIIIIIIIIII |                      "
    echo "                     IIIIIIIIII /    IIII /       IIII \   IIIII |      IIIII |  IIIIIIIIIIIIIII |                      "
    echo "                       IIIIIII /     IIII/        IIII  \  IIIII/       IIIII/   IIIIIIIIIIIIIII/                       "
    echo -e "\033[48;5;153m \033[38;5;24m                                                                                                                       "
    echo "                                                                                                                        "
    echo "                                                        OVER                                                            "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                        /.\/|                                                           "
    echo "                                                        \ /\|                                                           "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                                                                                        "

    sleep 2.5;
}

najlepszywynik()                        #funkcja ktora na prosbe gracza zwroci jego najlepszy wynik
{
    if [ $licznik -eq 0 ]; then
        echo "                                             Jeszcze ani razu nie zagrales :)" 
    else
        echo "                                             Twoj najlepszy wynik to : $maxrybki "
    fi     
}   


function zasady()
{
    clear
    for ((i=0;i<15;i++)) do
        echo -e "\033[48;5;153m \033[38;5;172m                                                                                                                       "
    done
        echo "                                          Aby spuscic wedke, wcisnij klawisz s                                          "
        echo "                                                                                                                        "
        echo "                                      Jezli chcesz zakonczyc gre wcisnij kaliwsz q                                      "
        echo "                                                                                                                        "
    for ((i=0;i<15;i++)) do
        echo -e "\033[48;5;153m \033[38;5;172m                                                                                                                       "
    done
    sleep 1
}   

function nowaryba()
{ 
    qqq=$[ RANDOM % 16 ];
    #czymozna=1;
    #for ((i=0;i<$ileryb;i++));do
    #    rrr=$[${nosyryb[$i]} % 120];
    #    if [ $rrr -gt 108 ]; then
    #        czymozna=0;
    #3    fi
    #done
    #if [ $czymozna -eq 1 ]; then
    ileryb=$[ileryb+1];
    nosyryb[$[ileryb-1]]=$[114+120*(qqq+1)];
   # fi
}

function nowapira()
{
    qq=$[ RANDOM % 12 ];
    ilepirek=$[ilepirek +1];
    nosypirani[$[ilepirek-1]]=$[112+120*(qq+3)];
}
function wedka()
{
    xwed=$[xwed+1];
    if [ $xwed -le 28 ]; then 
    for ((i=10;i<$xwed;i++));do
        tput cup $i 71;
        echo "|";
    done
    czyrybka=0;
 #   echo $xwed;
    wiersz=$[xwed-10];
    wsp=$[71+wiersz*120];
    tput cup 1 0;
   # echo $wsp;
    for((u=$lpl;u<$ilepirek;u++));do
        pir=$[${nosypirani[$u]}-240];
        
  #      echo $pir;
        if [ $wsp -gt $[pir-2] ];then
        if [ $[pir+8] -gt $wsp ];then
            gameover;
            zliczwyn;
 #           kon=1;
            return;
        fi 
        fi
    done
    for((i=$lll;i<$ileryb;i++));do
        ryb=${nosyryb[$i]};
        #tput cup $[i+3] 0;
        #echo $ryb;
        if [ $wsp -gt $[ryb-2] ] ;then
        if [ $[ryb+4] -gt $wsp ];then
         #   tput cup 0 0 
          #  echo TAK
            for ((ii=0;ii<2;ii++));do
            for ((j=0;j<11;j++));do
                tput cup $[xwed+ii] $[66+j];
                echo ' '
            done
            done
            nosyryb[$i]=-2;
            czyrybka=1;
            rybki=$[rybki+1];
            tput cup 2 18;
            echo $rybki;
            xwed=28;
        fi
        fi
    done
    if [ $czyrybka -eq 0 ];then
    tput cup $xwed 71;
    echo "J";
    else
    gggg=1;   
    fi
    fi
}

function akt()                          #aktualizacja planszy 
{
        if [ $xwed -eq 28 ];then
            for((y=28;y>9;y--));do
            tput cup $y 71;
            echo " ";
            done
            tput cup 10 71;
            echo J;
            trybwedki=0;
            xwed=10;
        fi
        for((j=$lll;j<$ileryb;j++));do
            
            if [ ${nosyryb[$j]} -ge -1 ];then
            nosyryb[$j]=$[${nosyryb[$j]}-1]
            k=${nosyryb[$j]};
            u=$[k % 120];
            zz=1;
            while [ $k -ge $[$zz*120] ];do
                zz=$[zz+1]
            done
            e=$[9+zz];
            tput cup $[e] $[u+5]
            echo ' '
            tput cup $[e+1] $[u+5]
            echo ' '
            tput cup $[e] $[u]
            rysujrybe2;
            if [ $u -eq 0 ]; then
                nosyryb[$j]=-2;
                tput cup $e $u
                echo '     '
                tput cup $[e+1] $u
                echo '     '
                lll=$[lll+1];
            fi
             fi
        done
        for((j=$lpl;j<$ilepirek;j++));do
            if [ ${nosypirani[$j]} -ge -1 ];then
            nosypirani[$j]=$[${nosypirani[$j]}-1]
            k=${nosypirani[$j]};
            u=$[k % 120];
            zz=1;
            while [ $k -ge $[$zz*120] ];do
            zz=$[zz+1]
            done
         e=$[9+zz]
         tput cup $e $[u+8]
         echo ' '
         tput cup $[e+1] $[u+8]
         echo ' '
         tput cup $[e-2] $[u+8]
         echo ' '
         tput cup $[e-1] $[u+8]
         echo ' '
         tput cup $e $u
         rysujpirke;
            if [ $u -eq 0 ]; then
                nosypirani[$j]=-2;
                tput cup $e $u
                echo '         '
                tput cup $[e+1] $[u]
                echo '         '
                tput cup $[e-2] $[u]
                echo '         '
                tput cup $[e-1] $[u]
                echo '         '
                lpl=$[lpl+1]
            fi
         fi
         done
    if [ $trybwedki -eq 1 ];then
        wedka;
    fi
}

licz=0;
function gra()
{
 kkkki=0;
 stty -echo
 zasady                                  #wypisuje zasady
 statek                                 #rysuje statek
 morze                                   #rysuje  morze
 tput cup 2 2;
 echo ZLOWIONE RYBKI: 0
 while :                                 #nieskoczona petla gry
 do
    if [ $kkkki -eq 0 ]; then
    if [ $rybki -eq 20 ]; then
        tput cup 0 0;
        statek2;
        morze;
        tput cup 2 2
        echo ZLOWIONE RYBKI: 5;
        kkkki=1;
    fi
    fi
    if [ $[licz % 15]  -eq 0 ];then
        if [ $[licz % 45 ] -eq 0 ]; then
            nowapira;
        else
            nowaryba;
        fi
    fi
    znak="y"; 
    read -t 0.01 -rsn1 -d '' znak
    if [ "$znak" == q ]; then
       zliczwyn;
       return;
    fi
    if [ "$znak" == s ]; then
        if [ $trybwedki -eq 0 ]; then
            trybwedki=1;
        fi
    fi
    akt;
    licz=$[licz+1];
    #if [ $kon -eq 1 ]; then
    #    return;
    #fi
 done
}                                        #tutaj bedzie ogolnie gra xd

function zliczwyn()                      #funkcja sprawdzjaca czy poprawil sie nasz najlepszy wynik
{
    if [ $rybki -gt $maxrybki ]; then
    maxrybki=$rybki;
    rybki=0;
    fi
    clear
    ekranpocz
}


koniec()                                #funckja zamykajaca gre i czyszczaca ekran !!!??$$$ bedzie trzeba dodac odstep czasowy
{
    clear
    gameover
    sleep 2
    tput setb 0;
    tput setf 7;
    clear
    stty echo;
    clear
    echo -e "\033[0m"
    exit 1;
}
function zmienne()
{
    licz=0;
    ileryb=0;
    ilepirek=0;
    trybwedki=0;
    xwed=10;
    znak="y"
    kon=0;
}
function ekranpocz
{
    zmienne
    wypisz
    x=2;
    while [ "$x" != "3" ] && [ "$x" != "1" ]; do
    read -rsn1 -d '' x                  #oczekiwanie na zmienna z klwaiatury
    if  [ $x == 2 ]; then
        wypisz                          #zeby rysowal bez poprzedniego wypisu
        najlepszywynik                  #wypisuje wynik
    fi
    done

    if [ $x == 3 ]; then
        koniec;
    fi
    
    if [ $x == 1 ]; then
        licznik=$[$licznik+1];
        gra
    fi

}
statek()
{
   echo -e "\033[48;5;153m \033[38;5;172m                                                                                                                   "
    echo "                                                                                                                        "
    echo "                                                                                                                        "
    echo "                                                               /|                                                       "
    echo "                                                              / |                                                       "
    echo "                                                             /  | o ___                                                 "
    echo "                                                            /___| \/   |                                                "
    echo "                                                                | /\   |                                                "
    echo -e "                                                            \033[48;5;241m         \033[48;5;153m  |                                               "
    echo -e "                                                             \033[48;5;241m       \033[48;5;153m   |                                             "

}
statek2()
{
    echo -e "\033[48;5;153m   \033[48;5;153m                                                                                                                 "
    echo -e "\033[48;5;153m  \033[48;5;153m                                                            /|                                                    "
    echo -e "\033[48;5;153m \033[48;5;153m                                                            / |                                                    "
    echo "                                                            /  |                                                        "
    echo "                                                           /   | o ____                                                 "
    echo "                                                          /____| \/    |                                                "
    echo "                                                               | /\    |                                                "
    echo -e "                                                   \033[48;5;241m TCS               \033[48;5;153m |                                               "
    echo -e "                                                    \033[48;5;241m                 \033[48;5;153m  |                                               "
    echo -e "                                                     \033[48;5;241m               \033[48;5;153m   |                                             "

}

morze()
{
   for (( j=0; j<2400; j=$[$j+1] )) ; do
      tabmorze[$j]=' '
   done
      tabmorze[71]='J'
   for (( jj=0; $jj<2400; jj=$[$jj+1] )) ; do
      echo -n -e "\033[48;5;24m\033[38;5;111m${tabmorze[$jj]}"
   done
}
rysujrybe2()
{   
  echo '/'
  tput cup $[e] $[u+1] 
  echo '.'
  tput cup $[e] $[u+2]
  echo '\'
  tput cup $[e] $[u+3]
  echo '/'
  tput cup $[e] $[u+4]
  echo '|'
  tput cup $[e+1] $[u]
  echo '\'
  tput cup $[e+1] $[u+1]
  echo '_'
  tput cup $[e+1] $[u+2]
  echo '/'
  tput cup $[e+1] $[u+3]
  echo '\'
  tput cup $[e+1] $[u+4]
  echo '|'
}
rysujpirke()
{
  echo 'V'
  tput cup $[e] $[u+1]
  echo 'v'
  tput cup $[e] $[u+2]
  echo 'v'
  tput cup $[e] $[u+3]
  echo ' '
  tput cup $[e] $[u+4]
  echo '/'
  tput cup $[e] $[u+5]
  echo '\'
  tput cup $[e] $[u+6]
  echo ' '
  tput cup $[e] $[u+7]
  echo '|'
  tput cup $[e-1] $[u]
  echo '/'
  tput cup $[e-1] $[u+1]
  echo 'o'
  tput cup $[e-1] $[u+2]
  echo ' '
  tput cup $[e-1] $[u+3]
  echo ' '
  tput cup $[e-1] $[u+4]
  echo '\'
  tput cup $[e-1] $[u+5]
  echo '/'
  tput cup $[e-1] $[u+6]
  echo ' '
  tput cup $[e-1] $[u+7]
  echo '|'
  tput cup $[e-2] $[u]
  echo ' '
  tput cup $[e-2] $[u+1]
  echo '_'
  tput cup $[e-2] $[u+2]
  echo '_'
  tput cup $[e-2] $[u+3]
  echo '_'
  tput cup $[e-2] $[u+4]
  echo ' '
  tput cup $[e-2] $[u+5]
  echo ' '
  tput cup $[e-2] $[u+6]
  echo '/'
  tput cup $[e-2] $[u+7]
  echo '|'
  tput cup $[e+1] $[u]
  echo '\'
  tput cup $[e+1] $[u+1]
  echo '_'
  tput cup $[e+1] $[u+2]
  echo '_'
  tput cup $[e+1] $[u+3]
  echo '_'
  tput cup $[e+1] $[u+4]
  echo '/'
  tput cup $[e+1] $[u+5]
  echo ' '
  tput cup $[e+1] $[u+6]
  echo '\'
  tput cup $[e+1] $[u+7]
  echo '|'
}
rysujrybe()
{
      for (( jj=0; $jj<$ileryb; jj=$[$jj+1] )) ; do
        wspolrzedna=nosyryb[$jj] 
          tabmorze[$wspolrzedna]='/'
           w1=$[$wspolrzedna+1]
              tabmorze[$w1]='.'
              w2=$[$wspolrzedna+2]
              tabmorze[w2]='\'
              w3=$[$wspolrzedna+3]
              tabmorze[w3]='/'
              w4=$[$wspolrzedna+4]
              tabmorze[w4]='|'
              w0d=$[$wspolrzedna+120]
              tabmorze[w0d]='\'
              w1d=$[$wspolrzedna+121]
              tabmorze[w1d]=' '
              w2d=$[$wspolrzedna+122]
              tabmorze[w2d]='/'
              w3d=$[$wspolrzedna+123]
              tabmorze[w3d]='\'
              w4d=$[$wspolrzedna+124]
             tabmorze[w4d]='|'
        done
}
gameover ()  
{
 licznik1=0
 rysujtlo   #ustawia dobry kolor tla i robi clear
 c=2
 d=59
 #tput cup $[c] $[d]
 #rysujczlowieka
 a=17
 b=56
 while :
 do
 c=2
 d=59
 tput cup $[c] $[d]
 rysujczlowieka
# for (( j=5; $j<18; j=$[$j+1] )) ; do
#   for (( jj=56; $jj<65; jj=$[$jj+1] )) ; do
#       tput cup $[j] $[jj]
#       echo ' '
#    done
 #done
 a=$[$a-1]
 b=56
 tput cup $[a] $[b]
 aktgo                                   #akrualizuje przesow ryby w gameover
 if [ $licznik1 -eq 13 ] ; then
   napisgameover
   return;
 fi
 sleep 0.01
 licznik1=$[$licznik1+1]
 done
}
rysujczlowieka ()
{
 echo 'o'
 tput cup $[c+1] $[d]
 echo '\'
 tput cup $[c+1] $[d+1]
 echo '/'
 tput cup $[c+2] $[d]
 echo '/'
 tput cup $[c+2] $[d+1]
 echo '\'
}

aktgo ()      #posuwanie pirani do gory game over
{
  echo ' '
  tput cup $[a] $[b+1]
  echo ' '
  tput cup $[a] $[b+2]
  echo ' '
  tput cup $[a] $[b+3]
  echo ' '
  tput cup $[a] $[b+4]
  echo ' '
  tput cup $[a] $[b+5]
  echo '_'
  tput cup $[a] $[b+6]
  echo ' '
  tput cup $[a] $[b+7]
  echo ' '
  tput cup $[a] $[b+8]
  echo ' '
  tput cup $[a+1] $[b]
  echo ' '
  tput cup $[a+1] $[b+1]
  echo ' '
  tput cup $[a+1] $[b+2]
  echo '_'
  tput cup $[a+1] $[b+3]
  echo ' '
  tput cup $[a+1] $[b+4]
  echo '/'
  tput cup $[a+1] $[b+5]
  echo ' '
  tput cup $[a+1] $[b+6]
  echo '\'
  tput cup $[a+1] $[b+7]
  echo ' '
  tput cup $[a+1] $[b+8]
  echo ' '
  tput cup $[a+2] $[b]
  echo ' '
  tput cup $[a+2] $[b+1]
  echo '/'
  tput cup $[a+2] $[b+2]
  echo ' '
  tput cup $[a+2] $[b+3]
  echo '>'
  tput cup $[a+2] $[b+4]
  echo '\'
  tput cup $[a+2] $[b+5]
  echo ' '
  tput cup $[a+2] $[b+6]
  echo ' '
  tput cup $[a+2] $[b+7]
  echo '\'
  tput cup $[a+2] $[b+8]
  echo ' '
  tput cup $[a+3] $[b]
  echo '|'
  tput cup $[a+3] $[b+1]
  echo ' '
  tput cup $[a+3] $[b+2]
  echo ' '
  tput cup $[a+3] $[b+3]
  echo '>'
  tput cup $[a+3] $[b+4]
  echo '<'
  tput cup $[a+3] $[b+5]
  echo ' '
  tput cup $[a+3] $[b+6]
  echo 'o'
  tput cup $[a+3] $[b+7]
  echo ' '
  tput cup $[a+3] $[b+8]
  echo '|'
  tput cup $[a+4] $[b]
  echo '|'
  tput cup $[a+4] $[b+1]
  echo ' '
  tput cup $[a+4] $[b+2]
  echo ' '
  tput cup $[a+4] $[b+3]
  echo '>'
  tput cup $[a+4] $[b+4]
  echo '<'
  tput cup $[a+4] $[b+5]
  echo ' '
  tput cup $[a+4] $[b+6]
  echo ' '
  tput cup $[a+4] $[b+7]
  echo ' '
  tput cup $[a+4] $[b+8]
  echo '|'
  tput cup $[a+5] $[b]
  echo '|'
  tput cup $[a+5] $[b+1]
  echo ' '
  tput cup $[a+5] $[b+2]
  echo ' '
  tput cup $[a+5] $[b+3]
  echo '\'
  tput cup $[a+5] $[b+4]
  echo '/'
  tput cup $[a+5] $[b+5]
  echo ' '
  tput cup $[a+5] $[b+6]
  echo ' '
  tput cup $[a+5] $[b+7]
  echo ' '
  tput cup $[a+5] $[b+8]
  echo '|'
  tput cup $[a+6] $[b]
  echo ' '
  tput cup $[a+6] $[b+1]
  echo '\'
  tput cup $[a+6] $[b+2]
  echo ' '
  tput cup $[a+6] $[b+3]
  echo ' '
  tput cup $[a+6] $[b+4]
  echo ' '
  tput cup $[a+6] $[b+5]
  echo ' '
  tput cup $[a+6] $[b+6]
  echo ' '
  tput cup $[a+6] $[b+7]
  echo '/'
  tput cup $[a+6] $[b+8]
  echo ' '
  tput cup $[a+7] $[b]
  echo ' '
  tput cup $[a+7] $[b+1]
  echo ' '
  tput cup $[a+7] $[b+2]
  echo '\'
  tput cup $[a+7] $[b+3]
  echo ' '
  tput cup $[a+7] $[b+4]
  echo ' '
  tput cup $[a+7] $[b+5]
  echo ' '
  tput cup $[a+7] $[b+6]
  echo '/'
  tput cup $[a+7] $[b+7]
  echo ' '
  tput cup $[a+7] $[b+8]
  echo ' '
  tput cup $[a+8] $[b]
  echo ' '
  tput cup $[a+8] $[b+1]
  echo ' '
  tput cup $[a+8] $[b+2]
  echo ' '
  tput cup $[a+8] $[b+3]
  echo '\'
  tput cup $[a+8] $[b+4]
  echo ' '
  tput cup $[a+8] $[b+5]
  echo '/'
  tput cup $[a+8] $[b+6]
  echo ' '
  tput cup $[a+8] $[b+7]
  echo ' '
  tput cup $[a+8] $[b+8]
  echo ' '
  tput cup $[a+9] $[b]
  echo ' '
  tput cup $[a+9] $[b+1]
  echo ' '
  tput cup $[a+9] $[b+2]
  echo ' '
  tput cup $[a+9] $[b+3]
  echo '/'
  tput cup $[a+9] $[b+4]
  echo ' '
  tput cup $[a+9] $[b+5]
  echo '\'
  tput cup $[a+9] $[b+6]
  echo ' '
  tput cup $[a+9] $[b+7]
  echo ' '
  tput cup $[a+9] $[b+8]
  echo ' '
  tput cup $[a+10] $[b]
  echo ' '
  tput cup $[a+10] $[b+1]
  echo ' '
  tput cup $[a+10] $[b+2]
  echo '/'
  tput cup $[a+10] $[b+3]
  echo ' '
  tput cup $[a+10] $[b+4]
  echo ' '
  tput cup $[a+10] $[b+5]
  echo ' '
  tput cup $[a+10] $[b+6]
  echo '\'
  tput cup $[a+10] $[b+7]
  echo ' '
  tput cup $[a+10] $[b+8]
  echo ' '
  tput cup $[a+11] $[b]
  echo ' '
  tput cup $[a+11] $[b+1]
  echo '/'
  tput cup $[a+11] $[b+2]
  echo '_'
  tput cup $[a+11] $[b+3]
  echo '_'
  tput cup $[a+11] $[b+4]
  echo '_'
  tput cup $[a+11] $[b+5]
  echo '_'
  tput cup $[a+11] $[b+6]
  echo '_'
  tput cup $[a+11] $[b+7]
  echo '\'
  tput cup $[a+11] $[b+8]
  echo ' '
  tput cup $[a+12] $[b]
  echo ' '
  tput cup $[a+12] $[b+1]
  echo ' '
  tput cup $[a+12] $[b+2]
  echo ' '
  tput cup $[a+12] $[b+3]
  echo ' '
  tput cup $[a+12] $[b+4]
  echo ' '
  tput cup $[a+12] $[b+5]
  echo ' '
  tput cup $[a+12] $[b+6]
  echo ' '
  tput cup $[a+12] $[b+7]
  echo ' '
  tput cup $[a+12] $[b+8]
  echo ' '
}

rysujtlo ()
{
     echo -n -e "\033[48;5;153m\033[38;5;172m "
    clear;
 }
###################################################
    clear
    ekranpocz
